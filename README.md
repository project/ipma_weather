# IPMA Weather

This module provides a weather block using IPMA API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ipma_weather).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ipma_weather).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

Drupal core block module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Once the module has been installed, navigate to
`admin/structure/block`
(Structure > Block Layout through the administration panel)
and add and configure the new block.


## Maintainers

- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)
